import java.util.Scanner;

public class Roulette {
  public static void main(String args[]) {
    Scanner scan = new Scanner(System.in);
    RouletteWheel wheel = new RouletteWheel();
    double money = 1000;
    boolean gameLoop = true;

    while (gameLoop) {
      System.out.println("\nWould you like to make a bet? Enter '1' for yes and '0' for no. \nCurrent money: " + money);
      int answer = scan.nextInt();

      while (answer < 0 || answer > 1) {
        System.out.println("Please try again! Enter ONLY '1' for yes and '0' for no.\nCurrent money " + money);
        answer = scan.nextInt();
      }

      if (answer == 0) {
        System.out.println("\nOkay. Bye bye.\nCurrent money: " + money);
        gameLoop = false;
      }
      else {
        System.out.println("\nHow much money do you want to bet? Current money: " + money);
        double bet = scan.nextDouble();

        while (bet > money || bet < 0) {
          System.out.println("\nError! Please enter again! Current money: " + money);
          bet = scan.nextDouble();
        }

        System.out.println("Enter a number you'd like to bet on from 0 - 36.");
        int numBet = scan.nextInt();

        while (numBet < 0 || numBet > 36) {
          System.out.println("Error! Please enter a number from 0 - 36!");
          numBet = scan.nextInt();
        }

        wheel.spin();
        int spinNum = wheel.getValue();
        System.out.println("\nYou picked the number: " + numBet + "\nSpin Number: " + spinNum);

        if (spinNum == numBet) {
          money += bet*35;
          System.out.println("Congratulations! you won!");
        }
        else {
          money -= bet;
          System.out.println("You lost! Better luck next time.");
        }
      }
    }
  }
}